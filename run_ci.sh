#!/bin/bash

set -uexo pipefail

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
RUN_IN_DOCKER="docker compose exec -e GITHUB_TOKEN -e PULUMI_CONFIG_PASSPHRASE -e CI_COMMIT_BRANCH -w /infrastructure/pulumi tooling"

try_creating_cluster() (
  set -e
  ${RUN_IN_DOCKER} npm ci
  ${RUN_IN_DOCKER} pulumi login --local --non-interactive
  ${RUN_IN_DOCKER} pulumi stack init ci --non-interactive
  set +x
  export GITHUB_TOKEN=$(${RUN_IN_DOCKER} pulumi config get github:token)
  set -x
  ${RUN_IN_DOCKER} pulumi config set flux_branch "${CI_COMMIT_BRANCH}"
  ${RUN_IN_DOCKER} bash -c 'PULUMI_K8S_DELETE_UNREACHABLE=true pulumi refresh --non-interactive --yes'
  ${RUN_IN_DOCKER} pulumi down --non-interactive --yes
  ${RUN_IN_DOCKER} pulumi up --non-interactive --yes

  # Wait for flux-system first
  ${RUN_IN_DOCKER} kubectl wait --for=condition=Ready kustomizations.kustomize.toolkit.fluxcd.io -n flux-system flux-system --timeout 20m
  # Then for all other kustomizations
  ${RUN_IN_DOCKER} kubectl wait --for=condition=Ready kustomizations.kustomize.toolkit.fluxcd.io -n flux-system --all --timeout 20m
  # Then for all helm releases
  ${RUN_IN_DOCKER} kubectl wait --for=condition=Ready helmrelease.helm.toolkit.fluxcd.io -n flux-system --all --timeout 20m
)

env

run() (
  if [ -z "${CI_COMMIT_BRANCH}" ]; then
    echo "branch has not been set, not running ci job"
    exit 1
  fi
  cd "$DIR"
  docker compose -f local-pullthrough-registries.vagrant.docker-compose.yaml up -d
  docker compose up -d tooling

  set +e
  try_creating_cluster
  RETVAL=$?
  ${RUN_IN_DOCKER} kubectl get kustomizations.kustomize.toolkit.fluxcd.io -n flux-system
  ${RUN_IN_DOCKER} kubectl get helmrelease.helm.toolkit.fluxcd.io -n flux-system
  set -e

  ${RUN_IN_DOCKER} pulumi down --non-interactive --yes
  exit $RETVAL
)

run
