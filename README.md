# Setting up pulumi s3 backend

```
$ aws configure --profile=digitalocean # Use secrets from sops file
$ pulumi login s3://cicd-workshop-pulumi-state?endpoint=fra1.digitaloceanspaces.com&profile=digitalocean
$ pulumi stack select digitalocean
$ pulumi refresh
```

# Setting up local k3d env

```
export PULUMI_CONFIG_PASSPHRASE=$(passwordmanger)
pulumi login
pulumi stack init dev
pulumi up
./install-certificate-from-pulumi-outputs.sh
```
