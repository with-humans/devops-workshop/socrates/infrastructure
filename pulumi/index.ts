import * as cryptoPackage from "crypto";
import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import createDigitalOcean from "./digitalocean";
import * as k3d from "./local-k3d";
import * as fs from "fs";
import * as tls from "@pulumi/tls";
import * as gitlab from "@pulumi/gitlab";
import * as random from "@pulumi/random";
import * as flux from "@worawat/flux";
import * as pulumiHtpasswd from "./htpasswd"
import { SelfSignedCertificateWithSAN } from "./selfSignedCertificate";

const run = async () => {
    const outputs: any = ({} as any);
    const config = new pulumi.Config();

    const kubernetes_provider = config.get("kubernetes_provider");

    let kubeconfig: pulumi.OutputInstance<string>,
        cluster: pulumi.Resource,
        kubeProvider: k8s.Provider;

    if (kubernetes_provider == "digitalocean") {
        ({ cluster, kubeconfig } = createDigitalOcean());
        kubeProvider = new k8s.Provider("k8s-provider", {
            kubeconfig: kubeconfig,
        });
    } else {
        const tmp = new k3d.K3dCluster("k3d", { name: "k3d" });
        cluster = tmp;
        kubeconfig = tmp.kubeconfig;
        kubeProvider = new k8s.Provider("k8s-provider", {
            kubeconfig: kubeconfig
        });
        outputs.ingressCaCert = tmp.ingressCaCert;
        outputs.ingressPem = tmp.ingressPem;
        outputs.ingressPrivateKey = tmp.ingressPrivateKey;
    }
    outputs.kubeconfig = kubeconfig;


    const fluxInstall = await flux.getFluxInstall({
        targetPath: config.get("flux_path")!,
        version: "v2.3.0",
        networkPolicy: false,
        logLevel: "debug",
    });

    // Create kubernetes resource from generated manifests
    const install = new k8s.yaml.ConfigGroup("flux-install", {
        yaml: fluxInstall.content,
        transformations: [(obj => {
            if (obj.kind !== "Deployment") return;

            if (obj.metadata.name === "kustomize-controller") {
                obj.spec.template.spec.containers[0].args.push("--requeue-dependency=1s")
                obj.spec.template.spec.containers[0].args.push("--kube-api-qps=100")
                obj.spec.template.spec.containers[0].args.push("--kube-api-burst=100")
                obj.spec.template.spec.containers[0].args.push("--min-retry-delay=250ms")
            } else if (obj.metadata.name === "helm-controller") {
                obj.spec.template.spec.containers[0].args.push("--requeue-dependency=1s")
                obj.spec.template.spec.containers[0].args.push("--kube-api-qps=100")
                obj.spec.template.spec.containers[0].args.push("--kube-api-burst=100")
                obj.spec.template.spec.containers[0].args.push("--min-retry-delay=250ms")
            }
        })]
    }, { provider: kubeProvider, deletedWith: cluster, dependsOn: cluster, retainOnDelete: true });

    // Create docker registry secret
    const dockerSecretUsername = "docker-registry-user"
    const dockerSecretPassword = new random.RandomPassword(`docker-secret-password`, {
        length: 32,
        special: false,
    });


    const dockerSecret = new k8s.core.v1.Secret("docker-registry-credentials", {
        metadata: {
            name: "docker-registry-credentials",
            namespace: "default"
        },
        stringData: pulumi.interpolate`${dockerSecretUsername}:${dockerSecretPassword.result}`.apply(auth => {
            const registry_host = config.get("registry_host");
            if (!registry_host) throw new Error("Missing config registry_host.");
            const data = JSON.stringify({
                auths: {
                    [registry_host]: {
                        auth: Buffer.from(auth).toString("base64")
                    }
                }
            });
            return { ".dockerconfigjson": data, "config.json": data }
        }),
        type: "kubernetes.io/dockerconfigjson"
    }, { provider: kubeProvider, dependsOn: [cluster] });

    const htpasswd = new pulumiHtpasswd.Htpasswd("docker-registry-htpasswd", {
        entries: [
            { username: dockerSecretUsername, password: dockerSecretPassword.result }
        ]
    });


    const dockerRegistryHtpasswdValueMap = new k8s.core.v1.Secret("docker-registry-htpasswd-secret", {
        metadata: {
            name: "docker-registry-htpasswd",
            namespace: "flux-system"
        },
        stringData: {
            "htpasswd": htpasswd.result
        }
    }, { provider: kubeProvider, dependsOn: [cluster, install], parent: dockerSecret })


    let sopsAgePrivateKey: pulumi.Resource | undefined;
    if (config.get("sops-age-private-key")) {
        sopsAgePrivateKey = new k8s.core.v1.Secret("sops-age", {
            metadata: {
                name: "sops-age",
                namespace: "flux-system"
            },
            stringData: {
                "sops.agekey": config.get("sops-age-private-key")!
            }
        }, { provider: kubeProvider, dependsOn: [cluster], deletedWith: cluster });
    }

    let linkerdCertSecret: pulumi.Resource | undefined;
    if (config.getBoolean("install_linkerd")) {
        linkerdCertSecret = prepareInstallLinkerd({ kubeProvider, cluster });
    }




    new k8s.yaml.ConfigGroup("flux-sync", {
        yaml: fs.readFileSync(config.get("flux_sync_path")!).toString(),
        transformations: [(obj => {
            if (obj.kind == "GitRepository" && obj.metadata.name == "flux-system") {
                obj.spec.ref.branch = config.get("flux_branch") || "main";
            }
        })]
    }, {
        provider: kubeProvider,
        dependsOn: [install, dockerRegistryHtpasswdValueMap, ...maybeLift(sopsAgePrivateKey), ...maybeLift(linkerdCertSecret)],
        deletedWith: cluster,
        retainOnDelete: true
    });


    [
        "with-humans/devops-workshop/socrates/ping/frontend",
        "with-humans/devops-workshop/socrates/slides",
        "with-humans/devops-workshop/socrates/airquality",
    ].forEach((repo) => {
        if (kubernetes_provider == "digitalocean") {
            createGitlabWebhook(repo, { kubeProvider, cluster });
        }
    });

    [
        "with-humans/devops-workshop/socrates/ping/frontend",
        "with-humans/devops-workshop/socrates/slides",
        "with-humans/devops-workshop/socrates/airquality",
        "with-humans/devops-workshop/socrates/infrastructure"
    ].forEach((repo) => {
        createDeployKey(repo, { kubeProvider, cluster });

        if (kubernetes_provider == "digitalocean") {
            createFluxHookSecret(repo, { kubeProvider, cluster, install })
        }
    });


    return outputs;
}

const outputs = run();
const extract = (key: string, prom: Promise<Record<string, any>>) => prom.then(r => r[key]);
export const kubeconfig = extract("kubeconfig", outputs);
export const ingressCaCert = extract("ingressCaCert", outputs);
export const ingressPem = extract("ingressPem", outputs);
export const ingressPrivateKey = extract("ingressPrivateKey", outputs);



const createFluxHookSecret = (repo: string, dependencies: {
    kubeProvider: k8s.Provider,
    cluster: pulumi.Resource,
    install: pulumi.Resource,
}) => {
    const { kubeProvider, cluster, install } = dependencies;
    const repoSlug = repo.replace(/\W/g, "-");
    const fluxHookSecret = new random.RandomPassword(`${repoSlug}-flux-hook-secret`, {
        length: 32,
        special: false,
    });

    const secret = new k8s.core.v1.Secret(`${repoSlug}-flux-system-hook-secret`, {
        metadata: {
            name: `${repoSlug}-flux-system-hook-secret`,
            namespace: "flux-system",
        },
        stringData: {
            "token": fluxHookSecret.result
        }
    }, { provider: kubeProvider, deletedWith: cluster, dependsOn: [cluster, install], retainOnDelete: true })

    // See https://fluxcd.io/flux/components/notification/receiver/#webhook-path
    const repoChecksum = fluxHookSecret.result.apply(token =>
        cryptoPackage
            .createHash('sha256')
            .update(`${token}${repoSlug}flux-system`)
            .digest('hex')
    );

    const projectHook = new gitlab.ProjectHook(`${repo}-flux-system-hook`, {
        project: repo,
        url: pulumi.interpolate`https://hooks.k8s.with-humans.org/flux/hook/${repoChecksum}`,
        pushEvents: true,
        enableSslVerification: true,
        token: fluxHookSecret.result,
    });

    return { fluxHookSecret, secret, projectHook };
}

const createDeployKey = (repo: string, { kubeProvider, cluster }: { kubeProvider: k8s.Provider, cluster: pulumi.Resource }) => {
    const repoSlug = repo.replace(/\W/g, "-");
    const deployKey = new tls.PrivateKey(`deploy-key-${repoSlug}`, {
        algorithm: "ED25519",
    });
    new gitlab.DeployKey(`${repoSlug}-deploy-key`, {
        title: `pulumi-${pulumi.getStack()}-deploy-key`,
        key: deployKey.publicKeyOpenssh,
        canPush: true,
        project: repo,
    });
    new k8s.core.v1.Secret(`${repoSlug}-deploy-key`, {
        metadata: {
            name: `${repoSlug}-deploy-key`,
            namespace: "default",
        },
        stringData: {
            "identity": deployKey.privateKeyOpenssh,
            "identity.pub": deployKey.publicKeyOpenssh,
            "known_hosts": `gitlab.com ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCsj2bNKTBSpIYDEGk9KxsGh3mySTRgMtXL583qmBpzeQ+jqCMRgBqB98u3z++J1sKlXHWfM9dyhSevkMwSbhoR8XIq/U0tCNyokEi/ueaBMCvbcTHhO7FcwzY92WK4Yt0aGROY5qX2UKSeOvuP4D6TPqKF1onrSzH9bx9XUf2lEdWT/ia1NEKjunUqu1xOB/StKDHMoX4/OKyIzuS0q/T1zOATthvasJFoPrAjkohTyaDUz2LN5JoH839hViyEG82yB+MjcFV5MU3N1l1QL3cVUCh93xSaua1N85qivl+siMkPGbO5xR/En4iEY6K2XPASUEMaieWVNTRCtJ4S8H+9
gitlab.com ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY=
gitlab.com ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAfuCHKVTjquxvt6CM6tdG4SLp1Btn/nOeHHE5UOzRdf`
        }
    }, { provider: kubeProvider, deletedWith: cluster, dependsOn: cluster, retainOnDelete: true });
}

function createGitlabWebhook(repo: string, { kubeProvider, cluster }: { kubeProvider: k8s.Provider, cluster: pulumi.Resource }) {
    const repoSlug = repo.replace(/\W/g, "-");
    const tektonHookSecret = new random.RandomPassword(`${repoSlug}-tekton-hook-secret`, {
        length: 32,
        special: false,
    });

    new k8s.core.v1.Secret(`${repoSlug}-tekton-hook-secret`, {
        metadata: {
            name: `${repoSlug}-tekton-hook-secret`,
            namespace: "default",
        },
        stringData: {
            "token": tektonHookSecret.result
        }
    }, { provider: kubeProvider, deletedWith: cluster, dependsOn: cluster, retainOnDelete: true });

    new gitlab.ProjectHook(`${repo}-tekton-hook`, {
        project: repo,
        url: pulumi.interpolate`https://hooks.k8s.with-humans.org/tekton/hook/${repoSlug}`,
        pushEvents: true,
        enableSslVerification: true,
        token: tektonHookSecret.result,
    });
}

function prepareInstallLinkerd({ kubeProvider, cluster }: { kubeProvider: k8s.Provider, cluster: pulumi.Resource }): pulumi.Resource | undefined {
    const namespace = new k8s.core.v1.Namespace("linkerd", {
        metadata: {
            name: "linkerd",
        }
    }, { provider: kubeProvider, deletedWith: cluster, dependsOn: cluster, retainOnDelete: true });

    const algorithm = "RSA";
    const rsaBits = 2048;
    const ecdsaCurve = "P224";
    // create a CA private key
    const caKey = new tls.PrivateKey(`linkerd-ca`, {
        algorithm,
        ecdsaCurve,
        rsaBits,
    });

    // create a CA certificate
    const caCert = new tls.SelfSignedCert(`linkerd-ca`, {
        privateKeyPem: caKey.privateKeyPem,
        isCaCertificate: true,
        validityPeriodHours: 24 * 365,
        subject: { commonName: "root.linkerd.cluster.local" },
        allowedUses: ["cert_signing", "crl_signing"]
    }, { parent: caKey });

    const configMap = new k8s.core.v1.ConfigMap("linkerd-identity-trust-roots", {
        metadata: {
            name: "linkerd-identity-trust-roots",
            namespace: namespace.metadata.name,
        },
        data: {
            "ca-bundle.crt": caCert.certPem,
        }
    }, { provider: kubeProvider, deletedWith: cluster, dependsOn: cluster });

    const secret = new k8s.core.v1.Secret("linkerd-trust-anchor", {
        metadata: {
            name: "linkerd-trust-anchor",
            namespace: namespace.metadata.name,
        },
        type: "kubernetes.io/tls",
        stringData: {
            "tls.crt": caCert.certPem,
            "tls.key": caKey.privateKeyPem,
        }
    }, { provider: kubeProvider, deletedWith: cluster, dependsOn: cluster })
    return secret;
}

const maybeLift = <T>(value: T | undefined): T[] => value ? [value] : [];

