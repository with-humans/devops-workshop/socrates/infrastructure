#!/bin/bash
set -euxo pipefail

tempFile="$(mktemp)"
pulumi stack  output --show-secrets --json | jq -r '.ingressCaCert' > $tempFile
step certificate install "$tempFile"
