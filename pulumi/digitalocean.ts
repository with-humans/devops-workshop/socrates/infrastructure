import * as pulumi from "@pulumi/pulumi";
import * as digitalocean from "@pulumi/digitalocean";
import * as flux from "@worawat/flux";
import * as k8s from "@pulumi/kubernetes";
import * as k8sc from "@kubernetes/client-node";

export default () => {
    const domain = new digitalocean.Domain("default", {
        name: "k8s.with-humans.org"
    });

    const cluster = new digitalocean.KubernetesCluster("do-cluster", {
        region: digitalocean.Region.FRA1,
        version: "1.30.2-do.0",
        destroyAllAssociatedResources: true,
        nodePool: {
            name: "default",
            size: digitalocean.DropletSlug.DropletS4VCPU8GB,
            nodeCount: 3,
        },
    } as any & digitalocean.KubernetesClusterArgs);

    const user = "admin";
    const config = new pulumi.Config("digitalocean");
    let kubeconfig = pulumi.interpolate`apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: ${cluster.kubeConfigs[0].clusterCaCertificate}
    server: ${cluster.endpoint}
  name: ${cluster.name}
contexts:
- context:
    cluster: ${cluster.name}
    user: ${cluster.name}-${user}
  name: ${cluster.name}
current-context: ${cluster.name}
kind: Config
users:
- name: ${cluster.name}-${user}
  user:
    token: ${config.getSecret("token")}
`;

    const kubeProvider = new k8s.Provider("local-k8s-provider", {
        kubeconfig,
    });

    const digitaloceanProject = new digitalocean.Project("cicd-workshop", {
        name: "CI/CD Workshop",
        environment: "Production",
        resources: [cluster.clusterUrn, domain.domainUrn]
    })

    if (!pulumi.runtime.isDryRun()) {
        const ip = pulumi.all([kubeconfig]).apply(async ([kcStr]) => {
            const kc = new k8sc.KubeConfig();
            kc.loadFromString(kcStr);
            const k8sApi = kc.makeApiClient(k8sc.CoreV1Api);

            pulumi.log.info("Waiting for ingress ip");
            while (true) {
                try {
                    const response = await k8sApi.readNamespacedService("traefik-traefik", "traefik");
                    return response.body.status!.loadBalancer!.ingress![0].ip!;
                } catch (e) {
                    await new Promise(r => setTimeout(r, 5000));
                }
            }
        });

        const anyRecord = new digitalocean.DnsRecord("any", {
            domain: domain.name,
            type: "A",
            name: "*",
            value: ip
        });

        const baseRecord = new digitalocean.DnsRecord("base", {
            domain: domain.name,
            type: "A",
            name: "@",
            value: ip
        });

        kubeconfig = kubeconfig.apply(async kcStr => {
            const kc = new k8sc.KubeConfig();
            kc.loadFromString(kcStr);
            const k8sApi = kc.makeApiClient(k8sc.AppsV1Api);

            pulumi.log.info("Waiting coredns to be ready");
            while (true) {
                try {
                    const response = await k8sApi.readNamespacedDeploymentStatus("coredns", "kube-system");
                    if (response.body.status?.conditions?.some(c => c.type === "Available" && c.status === "True")) {
                        break;
                    }
                } catch (e) {
                    await new Promise(r => setTimeout(r, 5000));
                }
            }


            return kcStr;
        })
    }

    return { kubeProvider, kubeconfig, cluster }

}

