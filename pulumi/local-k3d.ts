import { local } from "@pulumi/command";
import * as k8s from "@pulumi/kubernetes";
import * as pulumi from "@pulumi/pulumi";
import { SelfSignedCertificateWithSAN } from "./selfSignedCertificate";
import * as k8sc from "@kubernetes/client-node";

export interface LocalK3dClusterInputs {
  name: pulumi.Input<string>;
}

interface LocalK3dProviderInputs {
  name: string;
}

export class K3dCluster
  extends pulumi.ComponentResource<LocalK3dProviderInputs> {
  public name: pulumi.Output<string>;
  public kubeconfig: pulumi.Output<string>;
  public readonly ingressPem: pulumi.Output<string>;
  public readonly ingressPrivateKey: pulumi.Output<string>;
  public readonly ingressCaCert: pulumi.Output<string>;

  constructor(
    name: string,
    props: LocalK3dClusterInputs,
    opts?: pulumi.CustomResourceOptions,
  ) {
    super("k3d:cluster", name, props, opts);

    const pulumiCfg = new pulumi.Config();

    const create = pulumi.interpolate`k3d cluster create ${props.name} \
        --k3s-arg "--disable=traefik@server:*" \
        -p "80:80@loadbalancer" \
        -p "443:443@loadbalancer" \
        --registry-config "${
      pulumiCfg.get("k3s_mirros") || "./local-k3d/mirrors.yaml"
    }" \
        --wait`;

    const del = pulumi.interpolate`k3d cluster delete ${props.name}`;
    const command = new local.Command("k3d:create", {
      create,
      delete: del,
      update: pulumi.concat([del, create]),
    }, { parent: this, deleteBeforeReplace: true, replaceOnChanges: ["*"] });

    const config = new local.Command("k3d:config", {
      create: pulumi.interpolate`k3d kubeconfig get ${props.name}`,
    }, { parent: command, deletedWith: command });

    this.name = pulumi.Output.create(props.name);
    this.kubeconfig = pulumi.secret(config.stdout);
    const cert = new SelfSignedCertificateWithSAN("ingress-cert", {
      validityPeriodHours: 24 * 30,
      localValidityPeriodHours: 24 * 30,
      subject: { commonName: "host.k3d.internal" },
      dnsNames: [
        "localhost",
        "host.k3d.internal",
        "k3d.local.with-humans.org",
        "*.k3d.local.with-humans.org",
      ],
    });
    this.ingressPem = cert.pem;
    this.ingressPrivateKey = cert.privateKey;
    this.ingressCaCert = cert.caCert;

    const kubeProvider = new k8s.Provider("local-k8s-provider", {
      kubeconfig: this.kubeconfig,
    }, { parent: command, dependsOn: [command], deletedWith: command });

    const coreDnsCustom = new k8s.core.v1.ConfigMap("coredns-custom", {
      apiVersion: "v1",
      kind: "ConfigMap",
      metadata: {
        name: "coredns-custom",
        namespace: "kube-system",
      },
      data: {
        "ingress.server": `
              k3d.local.with-humans.org:53 {
                rewrite name regex .* host.k3d.internal
                forward . 127.0.0.1
              }
            `,
      },
    }, {
      provider: kubeProvider,
      parent: command,
      dependsOn: [command],
      deletedWith: command,
    });

    if (!pulumi.runtime.isDryRun()) {
      pulumi.all([this.kubeconfig, coreDnsCustom.id]).apply(async ([kcStr]) => {
        const kc = new k8sc.KubeConfig();
        kc.loadFromString(kcStr);
        const k8sApi = kc.makeApiClient(k8sc.AppsV1Api);
        await k8sApi.patchNamespacedDeployment(
          "coredns",
          "kube-system",
          {
            spec: {
              template: {
                metadata: {
                  annotations: {
                    "kubectl.kubernetes.io/restartedAt": new Date().toString(),
                  },
                },
              },
            },
          },
          undefined,
          undefined,
          undefined,
          undefined,
          undefined,
          {
            headers: {
              "Content-Type":
                k8sc.PatchUtils.PATCH_FORMAT_STRATEGIC_MERGE_PATCH,
            },
          },
        );
      });
    }

    if (pulumiCfg.get("ingress_type") === "istio") {
      const istioIngressNs = new k8s.core.v1.Namespace("istio-ingress", {
        metadata: {
          name: "istio-ingress",
          annotations: {
            "istio-injection": "enabled",
          },
        },
      }, {
        provider: kubeProvider,
        parent: command,
        dependsOn: [command],
        deletedWith: command,
      });
      new k8s.core.v1.Secret("istio-cert", {
        metadata: {
          namespace: "istio-ingress",
          name: "tls-default-certificate",
        },
        type: "kubernetes.io/tls",
        data: {
          "tls.crt": this.ingressPem.apply((pem) =>
            Buffer.from(pem).toString("base64")
          ),
          "tls.key": this.ingressPrivateKey.apply((pem) =>
            Buffer.from(pem).toString("base64")
          ),
        },
      }, { provider: kubeProvider, parent: command, dependsOn: [istioIngressNs] });
    } else {
      const traefikNs = new k8s.core.v1.Namespace("traefik", {
        metadata: {
          name: "traefik",
        },
      }, {
        provider: kubeProvider,
        parent: command,
        dependsOn: [command],
        deletedWith: command,
      });
      new k8s.core.v1.Secret("cert", {
        metadata: {
          namespace: traefikNs.metadata.name,
          name: "tls-default-certificate",
        },
        type: "kubernetes.io/tls",
        data: {
          "tls.crt": this.ingressPem.apply((pem) =>
            Buffer.from(pem).toString("base64")
          ),
          "tls.key": this.ingressPrivateKey.apply((pem) =>
            Buffer.from(pem).toString("base64")
          ),
        },
      }, { provider: kubeProvider, parent: command, dependsOn: [traefikNs] });
    }
  }
}
